# zapcfg [![go.dev reference](https://img.shields.io/badge/go.dev-reference-007d9c?logo=go&logoColor=white&style=flat-square)](https://pkg.go.dev/bitbucket.org/ai69/zapcfg)

Package zapcfg provides a collection of sophisticated uber-go/zap loggers.

## install

```bash
go get -d bitbucket.org/ai69/zapcfg
```
